package projet;


import data.DataListener;
import config.ConfigChart;
import data.Chart;
import data.DataTab;
import xml.XmlReaderWriter;
import java.io.File;
import java.util.ArrayList;

public class Observateur {
	static XmlReaderWriter xmlReaderWriter = new XmlReaderWriter();
	static DataListener dataListener = new DataListener();
	
	protected DataTab[] dataTabs;
	protected ArrayList<Chart> charts;
	protected ConfigChart[] configCharts;
	
	private ConfigChart[] lireXML(File file)
	/* Sp�cifications : Lit un ficher pour obtenir un COnfigChart[]
	* pr�-conditions : 
	* post-conditions : null si le fichier est vide ou n'existe pas */
	{
		return null;
	}
	
	public DataTab getDataTab(String label)
	/* Sp�cifications : Identifie un datatab par son label
	* pr�-conditions : �
	* post-conditions : null si le label n'est pas trouv� */
	{
		return null;
	}
        
        public void setCharts(ArrayList<Chart> charts)
        {
            this.charts = charts;
        }
	
        public void registerAllCharts()
        /* Specification : Demande a tous les charts de s'abonner au dataTab qui les intéressent
         *  Pre-conditions :
         * post-conditions : tous les charts sont abonnés
         */
        {
            for(Chart tmpChart : charts)
            {
                tmpChart.autoRegister();
            }
        }
	private void startEcoute()
	/* Sp�cifications : Lance l'�coute
	* pr�-conditions : �
	* post-conditions : � */
	{
		
	}
	
	protected void startSolver(String path, String[] args)
	/* Sp�cifications : lance l'ex�cution du solveur
	* pr�-conditions : l'�coute est lanc�e
	* post-conditions : path ne pointe pas vers un fichier: erreur */
	{
		
	}
	
	protected void stopSolver()
	/* Sp�cifications : Arrete l'ex�cution du solveur
	* pr�-conditions : Le solveur a �t� lanc�
	* post-conditions : S'il est en cours d'ex�cution, stoppe le solveur */
	{
		
	}
		
	protected void stop()
	/* Sp�cifications : Coupure g�n�rale
	* pr�-conditions : �
	* post-conditions : fermeture du logiciel et de toutes les d�pendances */
	{
		
	}


	
}
