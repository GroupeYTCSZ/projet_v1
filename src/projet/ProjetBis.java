/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet;

import data.Chart;
import config.ConfigSerie;
import config.ConfigPie;
import config.ConfigXY;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JDialog;
import org.jfree.chart.ChartPanel;
import view.ConfWindow;

/**
 *
 * @author Wouaip
 */
public class ProjetBis {

    /**
     * @param args the command line arguments
     */
    
    
    public static void main(String[] args) {
        
       //testXYChart();
       //testPieChart();
       Observateur obs = new Observateur();
       ConfWindow confWin = new ConfWindow(obs);
       confWin.setSize(1500, 1500);
       confWin.setVisible(true);

    }

    private static void testXYChart(Observateur obs) {
        ConfigSerie serie;
        serie = new ConfigSerie("SerieName1", Color.blue, true, true);
        ArrayList<ConfigSerie> serieList = new ArrayList<>();
        serieList.add(serie);
        
         serie = new ConfigSerie("SerieName2Bis", Color.green, true,false);
        serieList.add(serie);

        
         serie = new ConfigSerie("SerieName3Bis", Color.red, false, true);
        serieList.add(serie);
        
        serie = new ConfigSerie("SerieName4Bis", Color.yellow, false, false);
        serieList.add(serie);
        
        //ConfigXY graphConfig = new ConfigXY("TitreGraph", "xlabel", "yLabel", serieList);
        ConfigXY graphConfigLog = new ConfigXY("TitreGraphLog", "xlabel", "yLabel", serieList,true,10);
        ConfigXY graphConfig = new ConfigXY("TitreGraph", "xlabel", "yLabel", serieList);

        Chart myChart = new Chart(obs, graphConfig);

        
        createDialog(myChart);
        
            
        Chart myChart2 = new Chart(obs, graphConfigLog);
            
         createDialog(myChart2);
         
    }

    private static void testPieChart(Observateur obs) {
        
        ArrayList<String> datasetNameList = new ArrayList<>();
        datasetNameList.add("Dataset1");
        datasetNameList.add("Dataset2");
        datasetNameList.add("Dataset3");
        ConfigPie graphConfig = new ConfigPie("TitrePie", true, datasetNameList.size() , datasetNameList);

        Chart myChart = new Chart(obs, graphConfig);
        createDialog(myChart);
        
        
            
    }
    
    private static void createDialog(Chart myChart)
    {
         JDialog dialog = new JDialog();
            
            ChartPanel chartPanel = new ChartPanel(myChart.getChart());
            chartPanel.setPreferredSize(new java.awt.Dimension(500, 500));
            dialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                  //JDialog dialog = (JDialog) e.getWindow();
                  System.exit(0);
                }
            });
            dialog.setContentPane(chartPanel);
            dialog.setVisible(true);
            dialog.pack(); 
    }
}
